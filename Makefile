PYTHON := $(shell which python3)
VERSION := $(shell cat fizzgun/__init__.py | grep -o '[0-9]\+.[0-9]\+.[0-9]\+')
GIT_HASH := $(shell git rev-parse --short HEAD)

.PHONY: test test-unit test-e2e lint watch watch-e2e setup

test: lint test-unit test-e2e docs-build

test-unit: setup
	venv/bin/pytest tests/unit -v

test-e2e: setup
	venv/bin/pytest tests/e2e -v

lint: setup
	venv/bin/flake8 tests/ fizzgun/

watch: setup
	venv/bin/watchmedo shell-command -p "*.py;*.yaml" -W --recursive --command='pytest tests/unit -v' .

watch-e2e: setup
	venv/bin/watchmedo shell-command -p "*.py;*.yaml" -W --recursive --command='pytest tests/e2e -v' .

docs-serve: setup
	venv/bin/mkdocs serve

docs-build: setup
	venv/bin/mkdocs build

setup: venv/bin/activate

venv/bin/activate: requirements.txt dev_requirements.txt
	test -d venv || virtualenv -p python3 venv
	venv/bin/pip install -r requirements.txt
	venv/bin/pip install -r dev_requirements.txt
	touch venv/bin/activate
