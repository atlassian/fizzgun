import pytest

from tests.unit.support.builders.request_builder import RequestBuilder
from tests.unit.support.builders.scenarios.bubble_scenario_builder import BubbleScenarioBuilder
from tests.unit.support.fixtures.parametrize_tests import form_urlencoded_or_query
from tests.unit.support.fixtures.requests import a_json_request


@pytest.fixture()
def enlarger_scenario() -> BubbleScenarioBuilder:
    return BubbleScenarioBuilder('fizzgun.bubbles', 'Enlarger', 'name:enlarger')


def test_should_use_a_default_grow_factor_of_1000_in_json(enlarger_scenario: BubbleScenarioBuilder):
    mutations = (
        enlarger_scenario
        .when_input_request(a_json_request({'key': 'a'}))
    )
    assert len(mutations) == 1
    assert mutations[0].json_body == {'key': 'a' * 1000}


def test_should_support_redefinition_of_grow_factor_in_json(enlarger_scenario: BubbleScenarioBuilder):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(a_json_request({'key': 'b'}))
    )
    assert len(mutations) == 1
    assert mutations[0].json_body == {'key': 'bbbbb'}


def test_should_grow_a_default_string_if_json_string_value_is_empty(enlarger_scenario: BubbleScenarioBuilder):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(a_json_request({'key': ''}))
    )
    assert len(mutations) == 1
    assert mutations[0].json_body == {'key': 'aaaaa'}


def test_should_support_growing_integers_with_abs_value_of_one_or_less(enlarger_scenario: BubbleScenarioBuilder):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(a_json_request({'-1': -1, '0': 0, '1': 1}))
    )

    assert len(mutations) == 3
    assert any(mutation for mutation in mutations if mutation.json_body['-1'] < -1)
    assert any(mutation for mutation in mutations if mutation.json_body['0'] > 0)
    assert any(mutation for mutation in mutations if mutation.json_body['1'] > 1)


def test_should_support_growing_floats_with_abs_value_of_one_or_less(enlarger_scenario: BubbleScenarioBuilder):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(a_json_request({'-1.0': -1.0, '0.5': 0.5, '1.0': 1.0}))
    )

    assert len(mutations) == 3
    assert any(mutation for mutation in mutations if mutation.json_body['-1.0'] < -1)
    assert any(mutation for mutation in mutations if mutation.json_body['0.5'] > 1)
    assert any(mutation for mutation in mutations if mutation.json_body['1.0'] > 1)


def test_should_grow_integers_in_json(enlarger_scenario: BubbleScenarioBuilder):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(a_json_request({'key': 5}))
    )
    assert len(mutations) == 1
    assert mutations[0].json_body == {'key': 5 ** 5}


def test_should_grow_floats_in_json(enlarger_scenario: BubbleScenarioBuilder):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(a_json_request({'key': 5.1}))
    )
    assert len(mutations) == 1
    assert mutations[0].json_body == {'key': 5.1 ** 5}


def test_should_keep_sign_when_growing_negative_numbers_in_json(enlarger_scenario: BubbleScenarioBuilder):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(a_json_request({'key': -5}))
    )
    assert len(mutations) == 1
    assert mutations[0].json_body == {'key': -5 ** 5}


def test_should_grow_arrays_in_json(enlarger_scenario: BubbleScenarioBuilder):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(a_json_request({'key': [True]}))
    )
    assert len(mutations) == 1
    assert mutations[0].json_body == {'key': [True, True, True, True, True]}


def test_should_grow_nested_values_in_json(enlarger_scenario: BubbleScenarioBuilder):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(a_json_request({'key': {'key2': {'key3': 'a'}}}))
    )
    assert len(mutations) == 1
    assert mutations[0].json_body == {'key': {'key2': {'key3': 'aaaaa'}}}


def test_should_grow_each_array_element_in_json(enlarger_scenario: BubbleScenarioBuilder):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(a_json_request(['a', 2, {'key': 'b'}]))
    )
    assert len(mutations) == 4
    assert any(mutation for mutation in mutations if mutation.json_body == ['a', 2, {'key': 'b'}] * 5)
    assert any(mutation for mutation in mutations if mutation.json_body == ['aaaaa', 2, {'key': 'b'}])
    assert any(mutation for mutation in mutations if mutation.json_body == ['a', 2 ** 5, {'key': 'b'}])
    assert any(mutation for mutation in mutations if mutation.json_body == ['a', 2, {'key': 'bbbbb'}])


def test_should_not_grow_booleans_or_nulls_in_json(enlarger_scenario: BubbleScenarioBuilder):
    mutations = (
        enlarger_scenario
        .when_input_request(a_json_request({'bool_true': True, 'bool_false': False, 'null': None}))
    )
    assert not mutations


def test_should_grow_object_keys_in_json_if_explicitly_enabled(enlarger_scenario: BubbleScenarioBuilder):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_keys=True, grow_factor=5)
        .when_input_request(a_json_request({'key': None}))
    )
    assert len(mutations) == 1
    assert mutations[0].json_body == {'keykeykeykeykey': None}


@form_urlencoded_or_query
def test_should_grow_urlencoded_parameters_as_strings(
    enlarger_scenario: BubbleScenarioBuilder, request_with_params, params_in_mutant
):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(request_with_params(('query', '1')))
    )
    assert len(mutations) == 1
    assert params_in_mutant(mutations[0]) == {'query': ['11111']}


@form_urlencoded_or_query
def test_should_mutate_each_urlencoded_parameter_value_separately(
    enlarger_scenario: BubbleScenarioBuilder, request_with_params, params_in_mutant
):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(request_with_params(('q1', 'a'), ('q2', 'b')))
    )
    assert len(mutations) == 2
    assert any(mutation for mutation in mutations if params_in_mutant(mutation) == {'q1': ['aaaaa'], 'q2': ['b']})
    assert any(mutation for mutation in mutations if params_in_mutant(mutation) == {'q1': ['a'], 'q2': ['bbbbb']})


@form_urlencoded_or_query
def test_should_grow_duplicated_urlencoded_parameters_separately(
    enlarger_scenario: BubbleScenarioBuilder, request_with_params, params_in_mutant
):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(request_with_params(('query', 'a'), ('query', 'b')))
    )
    assert len(mutations) == 2
    assert any(mutation for mutation in mutations if params_in_mutant(mutation) == {'query': ['aaaaa', 'b']})
    assert any(mutation for mutation in mutations if params_in_mutant(mutation) == {'query': ['a', 'bbbbb']})


@form_urlencoded_or_query
def test_should_grow_a_default_string_if_urlencoded_parameter_value_is_empty(
    enlarger_scenario: BubbleScenarioBuilder, request_with_params, params_in_mutant
):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(request_with_params(('q', '')))
    )
    assert len(mutations) == 1
    assert params_in_mutant(mutations[0]) == {'q': ['aaaaa']}


@form_urlencoded_or_query
def test_should_grow_keys_in_urlencoded_parameter_if_explicitly_enabled(
    enlarger_scenario: BubbleScenarioBuilder, request_with_params, params_in_mutant
):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_keys=True, grow_factor=5)
        .when_input_request(request_with_params(('q', '1')))
    )
    assert len(mutations) == 2
    assert any(mutation for mutation in mutations if params_in_mutant(mutation) == {'qqqqq': ['1']})


def test_should_mutate_json_body_and_query_if_both_present(
    enlarger_scenario: BubbleScenarioBuilder
):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(
            RequestBuilder
            .with_query_args(('q', 'a'))
            .with_json_payload({'j': 'b'})
            .build()
        )
    )

    assert len(mutations) == 2
    assert any(mutation for mutation in mutations
               if mutation.query_args == {'q': ['aaaaa']} and mutation.json_body == {'j': 'b'})
    assert any(mutation for mutation in mutations
               if mutation.query_args == {'q': ['a']} and mutation.json_body == {'j': 'bbbbb'})


def test_should_mutate_urlencoded_body_and_query_if_both_present(
    enlarger_scenario: BubbleScenarioBuilder
):
    mutations = (
        enlarger_scenario
        .given_bubble_settings(grow_factor=5)
        .when_input_request(
            RequestBuilder
            .with_query_args(('query', 'a'))
            .with_www_form_urlencoded_payload(('body', 'b'))
            .build()
        )
    )

    assert len(mutations) == 2
    assert any(mutation for mutation in mutations
               if mutation.query_args == {'query': ['aaaaa']} and mutation.form_urlencoded_body == {'body': ['b']})
    assert any(mutation for mutation in mutations
               if mutation.query_args == {'query': ['a']} and mutation.form_urlencoded_body == {'body': ['bbbbb']})
