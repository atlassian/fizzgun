import pytest

from tests.unit.support.builders.request_builder import RequestBuilder
from tests.unit.support.builders.scenarios.bubble_scenario_builder import BubbleScenarioBuilder


@pytest.fixture()
def shellshock_scenario() -> BubbleScenarioBuilder:
    return BubbleScenarioBuilder('fizzgun.bubbles', 'Shellshock', 'name:shellshock')


def test_should_not_inject_a_shellshock_header_if_request_path_does_not_include_cgi(
    shellshock_scenario: BubbleScenarioBuilder
):
    incoming = RequestBuilder.with_path('/a/regular/path').build()
    mutations = shellshock_scenario.when_input_request(incoming)
    assert not mutations


def test_should_inject_a_shellshock_header_if_path_contains_cgi(shellshock_scenario: BubbleScenarioBuilder):
    incoming = RequestBuilder.with_path('/server/cgi-bin/test').build()
    mutations = shellshock_scenario.when_input_request(incoming)
    assert len(mutations) == 1
    assert mutations[0].has_header('x-fizzgun-ss')


def test_should_include_a_body_response_expectation(shellshock_scenario: BubbleScenarioBuilder):
    incoming = RequestBuilder.with_path('/server/cgi-bin/test').build()
    expectations = shellshock_scenario.collect_expectations().when_input_request(incoming)
    assert len(expectations) == 1
    assert expectations[0] == [{'field': 'status', 'op': 'be_in_ranges', 'args': ['0-499']},
                               {'field': 'body', 'not': True, 'op': 'include', 'args': ['sh3llsh0ck']}]
