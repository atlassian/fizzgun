import copy
from typing import Dict

from fizzgun.application.service_contexts import ReporterContext
from fizzgun.core import Reporter

from tests.unit.support.builders.request_builder import RequestBuilder
from tests.unit.support.builders.response_builder import ResponseBuilder
from tests.unit.support.mocks import MockFileSystem, MockFactory
from tests.unit.support.mocks.mock_connectors import MockInput, MockOutput


class ReporterScenarioBuilder(object):
    def __init__(self):
        self._mock_file_system = None
        self._report_config = None
        self._session_id = None
        self._original_request = None
        self._modified_request = None
        self._bubble_name = 'DefaultBubbleName'
        self._bubble_description = 'default bubble description'
        self._response = None
        self._errors = ['default error message']
        self._message_count = 1

    def with_mock_file_system(self, mock_file_system: MockFileSystem) -> 'ReporterScenarioBuilder':
        return self._copy_with(mock_file_system=mock_file_system)

    def given_report_config(self, report_config: Dict) -> 'ReporterScenarioBuilder':
        return self._copy_with(report_config=report_config)

    def given_message_contains_session_id(self, session_id) -> 'ReporterScenarioBuilder':
        return self._copy_with(session_id=session_id)

    def given_message_contains_original_request(self, request: Dict) -> 'ReporterScenarioBuilder':
        return self._copy_with(original_request=request)

    def given_message_contains_modified_request(self, request: Dict) -> 'ReporterScenarioBuilder':
        return self._copy_with(modified_request=request)

    def given_message_contains_response(self, response: Dict) -> 'ReporterScenarioBuilder':
        return self._copy_with(response=response)

    def given_message_contains_bubble_info(self, name, description) -> 'ReporterScenarioBuilder':
        return self._copy_with(bubble_name=name, bubble_description=description)

    def given_message_contains_error_messages(self, *errors):
        return self._copy_with(errors=list(errors))

    def given_message_is_received_times(self, times: int) -> 'ReporterScenarioBuilder':
        return self._copy_with(message_count=times)

    def when_reporter_is_executed(self) -> MockOutput or Exception:
        mock_file_system = self._mock_file_system or MockFactory.create_mock_file_system()
        message = {
            'modified': self._modified_request or RequestBuilder.build(),
            'original': self._original_request or RequestBuilder.build(),
            'response': self._response or ResponseBuilder().build(),
            'session_id': self._session_id,
            'errors': self._errors,
            'bubble': {'name': self._bubble_name, 'description': self._bubble_description}
        }

        inputq = MockInput([message] * self._message_count)
        outputq = MockOutput()

        Reporter(ReporterContext(mock_file_system), inputq, outputq, report_config=self._report_config).start()

        return outputq

    def _copy_with(self, **kwargs) -> 'ReporterScenarioBuilder':
        new = ReporterScenarioBuilder()
        new._mock_file_system = kwargs.get('mock_file_system', self._mock_file_system)
        new._report_config = copy.deepcopy(kwargs.get('report_config', self._report_config))
        new._original_request = copy.deepcopy(kwargs.get('original_request', self._original_request))
        new._modified_request = copy.deepcopy(kwargs.get('modified_request', self._modified_request))
        new._response = copy.deepcopy(kwargs.get('response', self._response))
        new._session_id = kwargs.get('session_id', self._session_id)
        new._bubble_name = kwargs.get('bubble_name', self._bubble_name)
        new._bubble_description = kwargs.get('bubble_description', self._bubble_description)
        new._errors = copy.deepcopy(kwargs.get('errors', self._errors))
        new._message_count = kwargs.get('message_count', self._message_count)

        return new
