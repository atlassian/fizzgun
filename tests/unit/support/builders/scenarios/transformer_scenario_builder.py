import copy
from typing import List, AnyStr, Dict, Tuple, Type

from fizzgun.application.service_contexts import TransformerContext
from fizzgun.bubbles import Bubble
from fizzgun.core import Transformer
from tests.unit.support.builders.config import BubblesConfigBuilder
from tests.unit.support.mocks import (
    MockIdGenerator, MockModuleImporter, MockStdoutWriter, MockRandomGenerator, MockFactory)
from tests.unit.support.mocks.mock_connectors import MockInput, MockOutput


class TransformerScenarioBuilder(object):

    def __init__(self):
        self._bubbles_config_builder = None
        self._mock_id_generator = None
        self._mock_module_importer = None
        self._mock_stdout_writer = None
        self._mock_random_generator = None
        self._bubble_packs = []
        self._incoming_requests = []

    def with_mock_id_generator(self, id_generator: MockIdGenerator) -> 'TransformerScenarioBuilder':
        return self._copy_with(mock_id_generator=id_generator)

    def with_mock_module_importer(self, module_importer: MockModuleImporter) -> 'TransformerScenarioBuilder':
        return self._copy_with(mock_module_importer=module_importer)

    def with_mock_sdtoud_writer(self, stdout_writer: MockStdoutWriter) -> 'TransformerScenarioBuilder':
        return self._copy_with(mock_stdout_writer=stdout_writer)

    def with_mock_random_generator(self, random_generator: MockRandomGenerator) -> 'TransformerScenarioBuilder':
        return self._copy_with(mock_random_generator=random_generator)

    def with_bubbles_config_builder(self, bubbles_config_builder: BubblesConfigBuilder) -> 'TransformerScenarioBuilder':
        return self._copy_with(bubbles_config_builder=bubbles_config_builder)

    def given_bubbles_are_loaded(self, *bubble_classes) -> 'TransformerScenarioBuilder':
        return self.given_bubble_packs_defined(('default.bubblepack', list(bubble_classes), None))

    def given_bubble_is_loaded(self, bubble_class, module_name=None, settings=None) -> 'TransformerScenarioBuilder':
        module_name = module_name or 'default.bubblepack'
        if settings:
            settings = {bubble_class.__name__: settings}

        return self.given_bubble_packs_defined((module_name, [bubble_class], settings))

    def given_bubble_packs_defined(
            self, *bubble_packs: Tuple[AnyStr, List[Type[Bubble]], Dict or None]) -> 'TransformerScenarioBuilder':
        return self._copy_with(bubble_packs=list(bubble_packs))

    def given_incoming_requests(self, *incoming_requests) -> 'TransformerScenarioBuilder':
        return self._copy_with(incoming_requests=list(incoming_requests))

    def when_transformer_is_executed(self) -> MockOutput or Exception:
        mock_module_importer = self._mock_module_importer or MockFactory.create_mock_module_importer()
        mock_id_generator = self._mock_id_generator or MockFactory.create_mock_id_generator()
        mock_stdout_writer = self._mock_stdout_writer or MockFactory.create_mock_stdout_writer()
        mock_random_generator = self._mock_random_generator or MockFactory.create_mock_random_generator()
        bubbles_config_builder = self._bubbles_config_builder or BubblesConfigBuilder()

        bubbles_config_builder = self._prepare_bubble_packs(mock_module_importer, bubbles_config_builder)
        bubbles_config = bubbles_config_builder.build()

        transformer_context = TransformerContext(id_generator=mock_id_generator,
                                                 module_importer=mock_module_importer,
                                                 stdout_writer=mock_stdout_writer,
                                                 random_generator=mock_random_generator)

        inputq = MockInput([{'original': copy.deepcopy(request)} for request in self._incoming_requests])
        outputq = MockOutput()
        try:
            Transformer(transformer_context, inputq, outputq, bubbles_config=bubbles_config).start()
        except Exception as e:
            return e
        return outputq

    def _prepare_bubble_packs(
            self, mock_module_importer: MockModuleImporter, bubble_config_builder: BubblesConfigBuilder
    ) -> BubblesConfigBuilder:

        if not self._bubble_packs:
            return bubble_config_builder

        config_packs = []
        for module_name, bubble_classes, settings in self._bubble_packs:
            module_conf = {'module': module_name}
            if settings:
                module_conf['settings'] = settings
            config_packs.append(module_conf)

            mock_module_importer.given_module_with_bubbles_exists(module_name, *bubble_classes)
        return bubble_config_builder.with_bubble_packs(*config_packs)

    def _copy_with(self, **kwargs) -> 'TransformerScenarioBuilder':
        new = TransformerScenarioBuilder()
        new._bubbles_config_builder = kwargs.get('bubbles_config_builder', self._bubbles_config_builder)
        new._mock_id_generator = kwargs.get('mock_id_generator', self._mock_id_generator)
        new._mock_module_importer = kwargs.get('mock_module_importer', self._mock_module_importer)
        new._mock_stdout_writer = kwargs.get('mock_stdout_writer', self._mock_stdout_writer)
        new._mock_random_generator = kwargs.get('mock_random_generator', self._mock_random_generator)
        new._bubble_packs = copy.deepcopy(kwargs.get('bubble_packs', self._bubble_packs))
        new._incoming_requests = copy.deepcopy(kwargs.get('incoming_requests', self._incoming_requests))
        return new
