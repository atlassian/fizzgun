import copy
from typing import Dict

from fizzgun.application.service_contexts import RequestorContext
from fizzgun.core import Requestor
from tests.unit.support.mocks import MockFactory, MockHttpClient
from tests.unit.support.mocks.mock_connectors import MockInput, MockOutput
from tests.unit.support.builders.request_builder import RequestBuilder
from tests.unit.support.builders.response_builder import ResponseBuilder


class RequestorScenarioBuilder(object):
    def __init__(self):
        self._mock_http_client = None
        self._request = None
        self._response = None
        self._extra_properties = None

    def with_mock_http_client(self, mock_http_client: MockHttpClient) -> 'RequestorScenarioBuilder':
        return self._copy_with(mock_http_client=mock_http_client)

    def given_message_contains_modified_request(self, request: Dict) -> 'RequestorScenarioBuilder':
        return self._copy_with(request=request)

    def given_http_client_returns_response(self, response: Dict) -> 'RequestorScenarioBuilder':
        return self._copy_with(response=response)

    def given_message_contains_extra_properties(self, **kwargs):
        return self._copy_with(extra_properties=kwargs)

    def when_requestor_is_executed(self) -> MockOutput or Exception:
        request = self._request or RequestBuilder.build()
        extra = self._extra_properties or {}
        message = {'modified': request}
        message.update(extra)

        response = self._response or ResponseBuilder().build()
        response_tuple = (response['status'], response['reason'], dict(response['headers']), response['body'])
        mock_http_client = self._mock_http_client or MockFactory.create_mock_http_client()
        mock_http_client.given_request_returns(response_tuple)

        context = RequestorContext(mock_http_client)

        inputq = MockInput([message])
        outputq = MockOutput()

        Requestor(context, inputq, outputq).start()

        return outputq

    def _copy_with(self, **kwargs) -> 'RequestorScenarioBuilder':
        new = RequestorScenarioBuilder()
        new._request = copy.deepcopy(kwargs.get('request', self._request))
        new._response = copy.deepcopy(kwargs.get('response', self._response))
        new._extra_properties = copy.deepcopy(kwargs.get('extra_properties', self._extra_properties))
        new._mock_http_client = kwargs.get('mock_http_client', self._mock_http_client)
        return new
