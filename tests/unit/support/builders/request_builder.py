import json
import urllib.parse
from typing import Tuple


from fizzgun.models import HttpRequestBuilder


class ExtendedHttpRequestBuilder(HttpRequestBuilder):
    def with_json_payload(self, body: str) -> 'ExtendedHttpRequestBuilder':
        return self.with_header('Content-Type', 'application/json').with_body(json.dumps(body))

    def with_www_form_urlencoded_payload(self, *query_args: Tuple[str, str]) -> 'ExtendedHttpRequestBuilder':
        payload = urllib.parse.urlencode(query_args, doseq=True)
        return self.with_header('Content-Type', 'application/x-www-form-urlencoded').with_body(payload)


RequestBuilder = (
    ExtendedHttpRequestBuilder()
    .with_method('GET')
    .with_url('http://example.com/')
    .with_http_version('HTTP/1.1')
    .with_body('')
)
