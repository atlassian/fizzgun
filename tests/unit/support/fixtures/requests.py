from typing import Dict, Tuple, AnyStr, List

from tests.unit.support.builders.request_builder import RequestBuilder

_ARG_DEFAULT = object()


def a_json_request(payload: Dict or List=_ARG_DEFAULT):
    payload = {'default': 'payload'} if payload is _ARG_DEFAULT else payload
    return RequestBuilder.with_json_payload(payload).build()


def a_request_with_query(*args: Tuple[AnyStr, AnyStr]):
    if not args:
        args = (('default', 'query_argument'),)
    return RequestBuilder.with_query_args(*args).build()


def a_www_form_urlencoded_request(*args: Tuple[AnyStr, AnyStr]):
    if not args:
        args = (('default', 'form_argument'),)
    return RequestBuilder.with_www_form_urlencoded_payload(*args).build()
