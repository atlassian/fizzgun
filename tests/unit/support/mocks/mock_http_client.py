import unittest.mock

from fizzgun.application.dependencies import HttpClient, HttpClientResponse


class MockHttpClient(HttpClient):
    def given_request_returns(self, response: HttpClientResponse):
        pass


def create_mock_http_client() -> MockHttpClient:
    mock_http_client = unittest.mock.create_autospec(spec=HttpClient, instance=True)

    def given_request_returns(response: HttpClientResponse):
        mock_http_client.request.return_value = response

    setattr(mock_http_client,
            MockHttpClient.given_request_returns.__name__, given_request_returns)

    return mock_http_client
