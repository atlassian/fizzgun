import uuid
import unittest.mock

from fizzgun.application.dependencies import IdGenerator


class MockIdGenerator(IdGenerator):
    def given_generate_returns(self, id: str):
        pass

    def given_generate_returns_unique_ids(self):
        pass


def create_mock_id_generator() -> MockIdGenerator:
    mock_id_generator = unittest.mock.create_autospec(spec=IdGenerator, instance=True)

    def given_generate_returns(id: str):
        mock_id_generator.generate.return_value = id

    def given_generate_returns_unique_ids():
        mock_id_generator.generate.side_effect = lambda: str(uuid.uuid4())

    setattr(mock_id_generator,
            MockIdGenerator.given_generate_returns.__name__, given_generate_returns)
    setattr(mock_id_generator,
            MockIdGenerator.given_generate_returns_unique_ids.__name__, given_generate_returns_unique_ids)

    return mock_id_generator
