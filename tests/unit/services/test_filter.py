import pytest

from tests.unit.support.builders.config.filters_config_builder import FiltersConfigBuilder
from tests.unit.support.builders.request_builder import RequestBuilder
from tests.unit.support.builders.scenarios import FilterScenarioBuilder
from tests.unit.support.mocks.mock_stdout_writer import MockStdoutWriter


def test_should_process_every_incoming_message(
    filter_scenario_builder: FilterScenarioBuilder,
):

    request1 = RequestBuilder.with_path('/request/1').build()
    request2 = RequestBuilder.with_path('/request/2').build()

    output = (
        filter_scenario_builder
        .given_incoming_requests(request1, request2)
        .when_filter_is_executed()
    )

    assert len(output.results) == 2
    assert output.results[0] == {'original': request1}
    assert output.results[1] == {'original': request2}


should_forward_request_when = {
    'no rules are set':
        (lambda f: f,
         lambda r: r),

    'matches star endpoint rule':
        (lambda f: f.with_endpoint_rules('*'),
         lambda r: r),

    'matches fix endpoint rule':
        (lambda f: f.with_endpoint_rules('test.example.com:80'),
         lambda r: r.with_host('test.example.com').with_port(80)),

    'matches regex endpoint rule':
        (lambda f: f.with_endpoint_rules(r'.*\.example\.com:8[0-9]'),
         lambda r: r.with_host('anything.example.com').with_port(88)),

    'matches any of the endpoint rules':
        (lambda f: f.with_endpoint_rules('test.example.com:80')
                    .with_endpoint_rules('staging.example.com:80')
                    .with_endpoint_rules('staging.example.com:443'),
         lambda r: r.with_host('staging.example.com').with_port(80)),

    'method matches single method in methods matcher':
        (lambda f: f.with_endpoint_rules('*', method=['POST']),
         lambda r: r.with_method('POST')),

    'method matches any method in methods matcher':
        (lambda f: f.with_endpoint_rules('*', method=['PUT', 'POST', 'DELETE']),
         lambda r: r.with_method('POST')),

    'path matches fix value in the path matcher':
        (lambda f: f.with_endpoint_rules('*', path='/some/path'),
         lambda r: r.with_path('/some/path')),

    'url with query matches the path':
        (lambda f: f.with_endpoint_rules('*', path='/some/path'),
         lambda r: r.with_path('/some/path').with_query('query=string')),

    'path matches regex in the path matcher':
        (lambda f: f.with_endpoint_rules('*', path=r'/some/(path|route)/.*'),
         lambda r: r.with_path('/some/route/66')),

    'url matches fix value in the path_with_query matcher':
        (lambda f: f.with_endpoint_rules('*', path_with_query='/some/path?query=string'),
         lambda r: r.with_path('/some/path').with_query("query=string")),

    'url matches regex in the path_with_query matcher':
        (lambda f: f.with_endpoint_rules('*', path_with_query='/some/path\?query\=(string|strong).*'),
         lambda r: r.with_path('/some/path').with_query("query=stronger")),

    'matches when all matches are successful':
        (lambda f: f.with_endpoint_rules('*', path='/some/path', method=['POST']),
         lambda r: r.with_path('/some/path').with_method('POST')),

    'matches when one endpoint rule satisfies all the matchers':
        (lambda f: f.with_endpoint_rules('test.com:443', path='/idempotent/path', method=['PUT', 'DELETE', 'GET'])
                    .with_endpoint_rules('test.com:443', path='/post/allowed', method=['POST']),
         lambda r: r.with_host('test.com').with_scheme('https').with_path('/post/allowed').with_method('POST')),

    'matches if any wrong matcher name is used':
        (lambda f: f.with_endpoint_rules('*', baz='foo'),
         lambda r: r)
}


should_forward_request_when_scenarios = pytest.mark.parametrize(
    'prepare_filters_config,prepare_http_request',
    [value for value in should_forward_request_when.values()],
    ids=[key for key in should_forward_request_when.keys()]
)


@should_forward_request_when_scenarios
def test_should_forward_request_when(
    filter_scenario_builder: FilterScenarioBuilder,
    mock_stdout_writer: MockStdoutWriter,
    prepare_filters_config, prepare_http_request
):
    filters_config = prepare_filters_config(FiltersConfigBuilder()).build()
    http_request = prepare_http_request(RequestBuilder).build()

    output = (
        filter_scenario_builder
        .with_mock_sdtoud_writer(mock_stdout_writer)
        .with_filters_config(filters_config)
        .given_incoming_requests(http_request)
        .when_filter_is_executed()
    )

    mock_stdout_writer.write.assert_called_once_with("Processing request for host '%s:%d'"
                                                     % (http_request['host'], http_request['port']))

    assert len(output.results) == 1


should_stop_request_when = {
    'does not match fix endpoint rule':
        (lambda f: f.with_endpoint_rules('test.example.com:80'),
         lambda r: r.with_host('staging.example.com').with_port(80)),

    'does not match regex endpoint rule':
        (lambda f: f.with_endpoint_rules(r'.*:443'),
         lambda r: r.with_port(80)),

    'endpoint does not match start in regex endpoint rule':
        (lambda f: f.with_endpoint_rules(r'a-test\.example\.com:\d+'),
         lambda r: r.with_host('test.example.com').with_port(80)),

    'endpoint does not match end in regex endpoint rule':
        (lambda f: f.with_endpoint_rules(r'test\.example\.com:80'),
         lambda r: r.with_host('test.example.com').with_port(800)),

    'matches none of the endpoint rules':
        (lambda f: f.with_endpoint_rules('test.example.com:80')
                    .with_endpoint_rules('staging.example.com:80')
                    .with_endpoint_rules('prod.example.com:443'),
         lambda r: r.with_host('staging.example.com').with_port(443)),

    'method matches none of the items in methods matcher':
        (lambda f: f.with_endpoint_rules('*', method=['GET', 'PUT', 'DELETE']),
         lambda r: r.with_method('POST')),

    'path does not match fix value in the path matcher':
        (lambda f: f.with_endpoint_rules('*', path='/some/path'),
         lambda r: r.with_path('/some/part')),

    'path does not match regex in the path matcher':
        (lambda f: f.with_endpoint_rules('*', path=r'/some/(path|route)/.*'),
         lambda r: r.with_path('/some/road/66')),

    'url does not match fix value in the path_with_query matcher':
        (lambda f: f.with_endpoint_rules('*', path_with_query='/some/path?query=string'),
         lambda r: r.with_path('/some/path').with_query("query=strong")),

    'url does not match regex in the path_with_query matcher':
        (lambda f: f.with_endpoint_rules('*', path_with_query='/some/path\?query\=(string|strong).*'),
         lambda r: r.with_path('/some/path').with_query("query=foo")),

    'does not match if not all matches are successful':
        (lambda f: f.with_endpoint_rules('*', path='/some/path', method=['POST']),
         lambda r: r.with_path('/some/path').with_method('PUT')),

    'does not match when none of the endpoint rules satisfies all the matchers':
        (lambda f: f.with_endpoint_rules('test.com:443', path='/idempotent/path', method=['PUT', 'DELETE', 'GET'])
                    .with_endpoint_rules('test.com:443', path='/post/allowed', method=['POST']),
         lambda r: r.with_host('test.com').with_scheme('https').with_path('/idempotent/path').with_method('POST'))
}

should_stop_request_when_scenarios = pytest.mark.parametrize(
    'prepare_filters_config,prepare_http_request',
    [value for value in should_stop_request_when.values()],
    ids=[key for key in should_stop_request_when.keys()]
)


@should_stop_request_when_scenarios
def test_should_stop_request_when(
    filter_scenario_builder: FilterScenarioBuilder,
    mock_stdout_writer: MockStdoutWriter,
    prepare_filters_config, prepare_http_request
):
    filters_config = prepare_filters_config(FiltersConfigBuilder()).build()
    http_request = prepare_http_request(RequestBuilder).build()

    output = (
        filter_scenario_builder
        .with_mock_sdtoud_writer(mock_stdout_writer)
        .with_filters_config(filters_config)
        .given_incoming_requests(http_request)
        .when_filter_is_executed()
    )

    mock_stdout_writer.write.assert_called_once_with("Request to host '%s:%d' filtered"
                                                     % (http_request['host'], http_request['port']))
    assert not output.results


should_stop_already_seen_request_when = {
    'matches signature with just scheme':
        (['scheme'],
         lambda r: r.with_scheme('http')),

    'matches signature with just method':
        (['method'],
         lambda r: r.with_method('GET')),

    'matches signature with just host':
        (['host'],
         lambda r: r.with_host('test.com')),

    'matches signature with just port':
        (['port'],
         lambda r: r.with_port(8080)),

    'matches signature with just path':
        (['path'],
         lambda r: r.with_path('/this/path')),

    'matches signature with just query':
        (['query'],
         lambda r: r.with_query('key1=value1')),

    'matches signature with path+query':
        (['uri'],
         lambda r: r.with_path('/this/path').with_query('key1=value1')),

    'matches combined signature':
        (['method', 'host', 'path'],
         lambda r: r.with_method('POST').with_host('test.com').with_path('/this/path'))
}

should_stop_already_seen_request_when_scenarios = pytest.mark.parametrize(
    'signature,update_request',
    [value for value in should_stop_already_seen_request_when.values()],
    ids=[key for key in should_stop_already_seen_request_when.keys()]
)


@should_stop_already_seen_request_when_scenarios
def test_should_stop_already_seen_request_when(
    filter_scenario_builder: FilterScenarioBuilder, signature, update_request
):
    a_request = RequestBuilder.with_method('PUT').with_url('http://example.com/test?query=string').with_body('test')
    a_very_different_request = RequestBuilder.with_method('GET').with_url('https://domain.com/another/path?q=1')

    request1 = update_request(a_request).build()
    request2 = update_request(a_very_different_request).build()

    output = (
        filter_scenario_builder
        .with_filters_config(FiltersConfigBuilder().with_endpoint_rules('*', unseen=signature).build())
        .given_incoming_requests(request1, request2, request1)
        .when_filter_is_executed()
    )

    assert len(output.results) == 1
    assert output.last == {'original': request1}


should_forward_unseen_request_when = {
    'does not match signature with just scheme':
        (['scheme'],
         lambda r1: r1.with_scheme('http'),
         lambda r2: r2.with_scheme('https')),

    'does not match signature with just method':
        (['method'],
         lambda r1: r1.with_method('GET'),
         lambda r2: r2.with_method('POST')),

    'does not match signature with just host':
        (['host'],
         lambda r1: r1.with_host('test.com'),
         lambda r2: r2.with_host('test.com.ar')),

    'does not match signature with just port':
        (['port'],
         lambda r1: r1.with_port(8080),
         lambda r2: r2.with_port(8081)),

    'does not match signature with just path':
        (['path'],
         lambda r1: r1.with_path('/this/path'),
         lambda r2: r2.with_path('/that/path')),

    'does not match signature with just query':
        (['query'],
         lambda r1: r1.with_query('key1=value1'),
         lambda r2: r2.with_query('key1=value2')),

    'does not match signature with path_with_query (query differs)':
        (['path_with_query'],
         lambda r1: r1.with_query('key1=value1'),
         lambda r2: r2.with_query('key2=value1')),

    'does not match signature with path_with_query (path differs)':
        (['path_with_query'],
         lambda r1: r1.with_path('/this/path'),
         lambda r2: r2.with_path('/that/path')),

    'does not match combined signature':
        (['scheme', 'method', 'host', 'port', 'path'],
         lambda r1: r1.with_port(443),
         lambda r2: r2.with_port(444))
}


should_forward_unseen_request_when_scenarios = pytest.mark.parametrize(
    'signature,update_request1,update_request2',
    [value for value in should_forward_unseen_request_when.values()],
    ids=[key for key in should_forward_unseen_request_when.keys()]
)


@should_forward_unseen_request_when_scenarios
def test_should_forward_unseen_request_when(
    filter_scenario_builder: FilterScenarioBuilder, signature, update_request1, update_request2
):
    a_request = RequestBuilder.with_method('PUT').with_url('http://example.com/test?query=string').with_body('test')

    request1 = update_request1(a_request).build()
    request2 = update_request2(a_request).build()

    output = (
        filter_scenario_builder
        .with_filters_config(FiltersConfigBuilder().with_endpoint_rules('*', unseen=signature).build())
        .given_incoming_requests(request1, request2, request1)
        .when_filter_is_executed()
    )

    assert len(output.results) == 2
    assert output.results == [{'original': request1}, {'original': request2}]
