from fizzgun.models.expectation import Expectations

from tests.unit.support.builders.scenarios import CmdBubblesScenarioBuilder
from tests.unit.support.builders.bubble_builder import BubbleBuilder
from tests.unit.support.mocks import MockFileSystem


def test_should_include_information_about_loaded_bubble(cmd_bubbles_scenario_builder: CmdBubblesScenarioBuilder):

    expectations = Expectations()
    expectations.expect('status').to.be_equal_to(204)
    expectations.expect('body').to.include('something')

    bubble = (
        BubbleBuilder()
        .with_name('MyBubble')
        .with_docstring('My magic bubble')
        .with_tags('first:tag', 'second:tag')
        .with_expectations(expectations)
        .build()
    )

    output = (
        cmd_bubbles_scenario_builder
        .given_bubble_is_loaded(bubble.bubble_class)
        .when_cmd_bubbles_is_executed()
    )

    assert output == "Name: MyBubble\n" \
                     "Description: My magic bubble\n" \
                     "Tags:\n" \
                     "  * first:tag\n" \
                     "  * second:tag\n" \
                     "Expectations:\n" \
                     "  * Expecting 'status' to be equal to [204]\n" \
                     "  * Expecting 'body' to include ['something']\n"


def test_should_include_information_about_all_builtin_bubbles_by_default(
    cmd_bubbles_scenario_builder: CmdBubblesScenarioBuilder
):
    output = cmd_bubbles_scenario_builder.when_cmd_bubbles_is_executed()

    assert 'Name: Enlarger' in output
    assert 'Name: Trimmer' in output
    assert 'Name: TypeChanger' in output
    assert 'Name: Injector' in output
    assert 'Name: Shellshock' in output
    assert output.count('Name:') == 5


def test_should_not_load_any_configuration_file_when_custom_config_option_not_passed(
    cmd_bubbles_scenario_builder: CmdBubblesScenarioBuilder, mock_file_system
):
    (
        cmd_bubbles_scenario_builder
        .with_mock_file_system(mock_file_system)
        .when_cmd_bubbles_is_executed()
    )

    assert not mock_file_system.read_file.called


def test_should_load_configuration_file_when_custom_config_option_passed(
    cmd_bubbles_scenario_builder: CmdBubblesScenarioBuilder, mock_file_system
):
    (
        cmd_bubbles_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_config_path_is_passed('/use/my/config.yaml')
        .when_cmd_bubbles_is_executed()
    )

    mock_file_system.read_file.assert_called_once_with('/use/my/config.yaml')


def test_should_load_fizzgun_yaml_if_exists_in_cwd_and_not_config_option_passed(
    cmd_bubbles_scenario_builder: CmdBubblesScenarioBuilder, mock_file_system: MockFileSystem
):
    mock_file_system.given_files_exist('fizzgun.yaml')
    (
        cmd_bubbles_scenario_builder
        .with_mock_file_system(mock_file_system)
        .when_cmd_bubbles_is_executed()
    )

    mock_file_system.is_file.assert_called_once_with('fizzgun.yaml')
    mock_file_system.read_file.assert_called_once_with('fizzgun.yaml')


def test_should_look_for_fizzgun_yml_if_fizzgun_yaml_does_not_exist(
    cmd_bubbles_scenario_builder: CmdBubblesScenarioBuilder, mock_file_system: MockFileSystem
):
    mock_file_system.given_files_exist('fizzgun.yml')
    (
        cmd_bubbles_scenario_builder
        .with_mock_file_system(mock_file_system)
        .when_cmd_bubbles_is_executed()
    )

    assert mock_file_system.is_file.call_count == 2
    mock_file_system.is_file.assert_any_call('fizzgun.yaml')
    mock_file_system.is_file.assert_any_call('fizzgun.yml')

    mock_file_system.read_file.assert_called_once_with('fizzgun.yml')
