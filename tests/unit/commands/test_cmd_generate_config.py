from tests.unit.support.builders.scenarios import CmdGenerateConfigScenarioBuilder
from tests.unit.support.mocks import MockStdoutWriter, MockFileSystem
from tests.unit.support.util import StringMatching


def test_should_use_a_default_destination_filename_when_no_option_is_passed(
    cmd_generate_config_scenario_builder: CmdGenerateConfigScenarioBuilder,
    mock_stdout_writer: MockStdoutWriter,
    mock_file_system: MockFileSystem
):

    (
        cmd_generate_config_scenario_builder
        .with_mock_file_system(mock_file_system)
        .with_mock_sdtoud_writer(mock_stdout_writer)
        .when_cmd_generate_config_is_executed()
    )

    mock_file_system.copy_file.assert_called_once_with(StringMatching('config_template.yaml$'), 'fizzgun.yaml')
    mock_stdout_writer.write.assert_called_once_with(StringMatching('^File generated at: .*fizzgun.yaml$'))


def test_should_use_a_custom_destination_filename_when_passed(
    cmd_generate_config_scenario_builder: CmdGenerateConfigScenarioBuilder,
    mock_stdout_writer: MockStdoutWriter,
    mock_file_system: MockFileSystem
):

    (
        cmd_generate_config_scenario_builder
        .with_mock_file_system(mock_file_system)
        .with_mock_sdtoud_writer(mock_stdout_writer)
        .given_filename_is_passed('/custom/location/config.yaml')
        .when_cmd_generate_config_is_executed()
    )

    mock_file_system.copy_file.assert_called_once_with(
        StringMatching('config_template.yaml$'), '/custom/location/config.yaml')

    mock_stdout_writer.write.assert_called_once_with('File generated at: /custom/location/config.yaml')


def test_should_not_create_directories_when_passed_filename_does_not_include_paths(
    cmd_generate_config_scenario_builder: CmdGenerateConfigScenarioBuilder,
    mock_file_system: MockFileSystem
):

    (
        cmd_generate_config_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_filename_is_passed('just_filename.yaml')
        .when_cmd_generate_config_is_executed()
    )

    assert not mock_file_system.create_directory.called


def test_should_create_directories_when_passed_filename_includes_paths(
    cmd_generate_config_scenario_builder: CmdGenerateConfigScenarioBuilder,
    mock_file_system: MockFileSystem
):

    (
        cmd_generate_config_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_filename_is_passed('/path/to/new/config.yaml')
        .when_cmd_generate_config_is_executed()
    )

    mock_file_system.create_directory.assert_called_with('/path/to/new')


def test_should_create_a_file_with_default_settings_if_defaults_flag_is_set(
        cmd_generate_config_scenario_builder: CmdGenerateConfigScenarioBuilder,
        mock_file_system: MockFileSystem
):
    (
        cmd_generate_config_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_defaults_flag_is_set()
        .when_cmd_generate_config_is_executed()
    )

    mock_file_system.copy_file.assert_called_once_with(StringMatching('config_defaults.yaml$'), 'fizzgun.yaml')
