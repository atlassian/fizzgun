import copy
import urllib.parse

import requests

DEFAULT_METHOD = 'GET'
DEFAULT_BASE_URL = 'http://localhost'
DEFAULT_PATH = '/'
DEFAULT_BODY = None
DEFAULT_HEADERS = {}
DEFAULT_PROXY_SETTINGS = {}


class RequestGenerator(object):

    def __init__(self, method=None, base_url=None, path=None, headers=None, body=None, proxies=None):
        self._method = method or DEFAULT_METHOD
        self._base_url = base_url or DEFAULT_BASE_URL
        self._path = path or DEFAULT_PATH
        self._headers = copy.deepcopy(headers or DEFAULT_HEADERS)
        self._body = body or DEFAULT_BODY
        self._proxies = copy.deepcopy(proxies or {})

    def with_method(self, method) -> 'RequestGenerator':
        return RequestGenerator(method, self._base_url, self._path, self._headers, self._body, self._proxies)

    def with_base_url(self, base_url) -> 'RequestGenerator':
        return RequestGenerator(self._method, base_url, self._path, self._headers, self._body, self._proxies)

    def with_path(self, path) -> 'RequestGenerator':
        return RequestGenerator(self._method, self._base_url, path, self._headers, self._body, self._proxies)

    def with_headers(self, headers) -> 'RequestGenerator':
        return RequestGenerator(self._method, self._base_url, self._path, headers, self._body, self._proxies)

    def with_body(self, body) -> 'RequestGenerator':
        return RequestGenerator(self._method, self._base_url, self._path, self._headers, body, self._proxies)

    def with_proxy_auth(self, user, password) -> 'RequestGenerator':
        proxies = self._proxies.copy()
        proxies['auth'] = '%s:%s@' % (urllib.parse.quote(user), urllib.parse.quote(password))
        return RequestGenerator(self._method, self._base_url, self._path, self._headers, self._body, proxies)

    def with_http_proxy(self, proxy_port) -> 'RequestGenerator':
        proxies = self._proxies.copy()
        proxies['http'] = '127.0.0.1:%d' % proxy_port
        return RequestGenerator(self._method, self._base_url, self._path, self._headers, self._body, proxies)

    def with_https_proxy(self, proxy_port) -> 'RequestGenerator':
        proxies = self._proxies.copy()
        proxies['https'] = '127.0.0.1:%d' % proxy_port
        return RequestGenerator(self._method, self._base_url, self._path, self._headers, self._body, proxies)

    def send(self):
        s = requests.session()
        url = self._base_url + self._path
        request = requests.Request(method=self._method, url=url, headers=self._headers, data=self._body)
        request = s.prepare_request(request)
        print(self._build_proxy_settings())
        return s.send(request, proxies=self._build_proxy_settings())

    def _build_proxy_settings(self):
        proxies = {}
        proxy_auth = self._proxies.get('auth', '')
        http_proxy = self._proxies.get('http')
        https_proxy = self._proxies.get('https')

        if http_proxy:
            proxies['http'] = 'http://%s%s' % (proxy_auth, http_proxy)
        if https_proxy:
            proxies['https'] = 'http://%s%s' % (proxy_auth, https_proxy)
        return proxies
