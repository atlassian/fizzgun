import copy
import tempfile

import yaml

from fizzgun.config.defaults import defaults


class ConfigBuilder(object):
    def __init__(self, initial=None):
        self._cfg = initial or defaults()

    def with_stack_queue_type(self, stack_queue_type) -> 'ConfigBuilder':
        cfg = self._copy
        cfg['stack']['queue_type'] = stack_queue_type
        return ConfigBuilder(cfg)

    def with_processes_enabled(self) -> 'ConfigBuilder':
        cfg = self._copy
        cfg['stack']['use_processes'] = True
        return ConfigBuilder(cfg)

    def with_processes_disabled(self) -> 'ConfigBuilder':
        cfg = self._copy
        cfg['stack']['use_processes'] = False
        return ConfigBuilder(cfg)

    def with_report_directory(self, directory) -> 'ConfigBuilder':
        cfg = self._copy
        cfg['report']['directory'] = directory
        return ConfigBuilder(cfg)

    def with_mitmproxy_port(self, port) -> 'ConfigBuilder':
        cfg = self._copy
        cfg['mitmproxy']['port'] = port
        return ConfigBuilder(cfg)

    def with_bubble_pack(self, bubble_pack_config) -> 'ConfigBuilder':
        cfg = self._copy
        cfg['bubbles']['bubble-packs'] = [copy.deepcopy(bubble_pack_config)]
        return ConfigBuilder(cfg)

    def value(self):
        return self._copy

    @property
    def mitmproxy_port(self):
        return self._cfg['mitmproxy']['port']

    @property
    def as_tempfile(self):
        with tempfile.NamedTemporaryFile(mode='w', suffix='fizzgun.yaml', delete=False) as f:
            yaml.dump(self._cfg, f.file)
        return f.name

    @property
    def _copy(self):
        return copy.deepcopy(self._cfg)
