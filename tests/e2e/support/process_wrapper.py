from multiprocessing import Process


class ProcessWrapper(object):

    def __init__(self, target, *args, on_enter=None, **kwargs):
        self._p = Process(target=target, args=args, kwargs=kwargs)
        self._on_enter = on_enter

    def __enter__(self):
        self.start()
        self._on_enter and self._on_enter()
        return self._p

    def __exit__(self, *args):
        self.terminate()

    def start(self):
        self._p.start()

    def terminate(self):
        self._p.terminate()
