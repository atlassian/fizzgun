import contextlib
import os.path
import tempfile
import time

import pytest
from fizzgun.bin import cli

from tests.e2e.support.process_wrapper import ProcessWrapper
from tests.e2e.support.config_builder import ConfigBuilder
from tests.e2e.support.request_generator import RequestGenerator
from tests.e2e.conftest import wait_for, is_http_server_ready


@contextlib.contextmanager
def given_fizzgun_is_running_with_config(config_builder):
    proxy_port = config_builder.mitmproxy_port
    config = config_builder.as_tempfile

    with ProcessWrapper(cli.main, ['run', '-c', config],
                        on_enter=lambda: wait_for(lambda: is_http_server_ready(proxy_port), timeout=5)):
        yield proxy_port


def unique_reports_dir():
    return os.path.join(tempfile.TemporaryDirectory().name, 'reports')


fizzgun_deployment_scenarios = pytest.mark.parametrize(
    'initial_config',
    [
        (ConfigBuilder().with_processes_disabled().with_stack_queue_type('zmq')),
        (ConfigBuilder().with_processes_enabled().with_stack_queue_type('zmq')),
        (ConfigBuilder().with_processes_disabled().with_stack_queue_type('python')),
        (ConfigBuilder().with_processes_enabled().with_stack_queue_type('python'))
    ],
    ids=['ZMQ w/threads', 'ZMQ w/processes', 'Python Queues w/threads', 'Python Queues w/processes'])


@fizzgun_deployment_scenarios
def test_should_mutate_incoming_requests_and_report_bugs(web_app, initial_config: ConfigBuilder):
    report_dir = unique_reports_dir()

    config_builder = initial_config.with_report_directory(report_dir)

    with given_fizzgun_is_running_with_config(config_builder) as proxy_port:
        web_app.will_return_response(status=500, body='oops something went wrong')
        request_generator = (
            RequestGenerator()
            .with_http_proxy(proxy_port)
            .with_proxy_auth('test-session', 'pwd')
            .with_method('POST')
            .with_base_url(web_app.base_url)
            .with_path('/a/cgi-bin/?query=string')
            .with_headers({'Content-Type': 'application/json'})
            .with_body('{"body": "123"}')
        )
        request_generator.send()

        expected_report_path = os.path.join(report_dir, 'test-session.txt')

        wait_for(lambda: os.path.isfile(expected_report_path), timeout=5)

        wait_for(lambda: time.time() - os.path.getmtime(expected_report_path) > 1, timeout=5)

        with open(expected_report_path) as f:
            content = f.read()

    expected_error_count = 9
    assert "Expecting 'status' to be in ranges ['0-499'] (actual: 500)" in content
    assert content.count("Expecting 'status' to be in ranges ['0-499'] (actual: 500)") == expected_error_count

    assert 'Bubble: Trimmer' in content
    assert content.count('Bubble: Trimmer') == 2

    assert 'Bubble: Enlarger' in content
    assert content.count('Bubble: Enlarger') == 2

    assert 'Bubble: TypeChanger' in content
    assert content.count('Bubble: TypeChanger') == 2

    assert 'Bubble: Injector' in content
    assert content.count('Bubble: Injector') == 2

    assert 'Bubble: Shellshock' in content
    assert content.count('Bubble: Shellshock') == 1


def test_should_serve_a_web_utility_when_requesting_domain_fizzgun_it():
    report_dir = unique_reports_dir()

    os.makedirs(report_dir)

    with open(os.path.join(report_dir, 'dummy.txt'), 'w+') as fh:
        fh.write('dummy report content')

    config = ConfigBuilder().with_processes_disabled().with_stack_queue_type('python').with_report_directory(report_dir)

    with given_fizzgun_is_running_with_config(config) as proxy_port:
        fizzgun_web = (
            RequestGenerator()
            .with_method('GET')
            .with_http_proxy(proxy_port)
            .with_proxy_auth('test', 'pwd')
            .with_base_url('http://fizzgun.it')
        )

        # Home
        response = fizzgun_web.with_path('/').send().text
        assert 'Fizzgun - Powered by mitmproxy' in response
        assert '<a href="/cert/pem">' in response
        assert '<a href="/cert/p12">' in response

        # Certs
        assert fizzgun_web.with_path('/cert/pem').send().status_code == 200
        assert fizzgun_web.with_path('/cert/p12').send().status_code == 200

        # Reports
        response = fizzgun_web.with_path('/reports/').send().text
        assert '<h2>Reports</h2>' in response
        assert '<li><a href="dummy.txt">dummy.txt</a></li>' in response

        response = fizzgun_web.with_path('/reports/dummy.txt').send().text
        assert response == 'dummy report content'
