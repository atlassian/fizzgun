# Fizzgun - A Simple and Effective HTTP Fuzzer

![logo](docs/img/logo.png)

Fizzgun (anagram of *Fuzzing*) is an HTTP(S) Fuzzer that automates negative testing for your API/Front-end service by
creating mutants of real time requests taken from your api tests, selenium tests, or your manual exploratory browsing
session. It's very effective and simple to set up.

## Installation

Fizzgun requires Python 3.5 or newer.

`pip install -U fizzgun`


## Documentation

Access the full documentation, and examples at [http://fizzgun.readthedocs.io](http://fizzgun.readthedocs.io)

## Contributing

See [docs/contributing](wiki/contributing.md)

## License

See [LICENSE](LICENSE)
