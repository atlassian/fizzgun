### HttpRequest

 When [creating your own bubbles](bubble-creation.md) the argument to the `generate` and `does_apply` methods is an instance of an `HttpRequest`.

Wraps a dict-like request specification and provides utilities around it

**Properties**:

  * **body →** *str* : The request body
  * **endpoint →** *str* : The host:port tuple for this request
  * **form_urlencoded_body →** *Dict* : The body parsed as a urlencoded form
  * **headers →** *Dict* : The request headers
  * **host →** *str* : The host section of the request URL
  * **json_body →** *Any* : The body parsed as json
  * **method →** *str* : The request method
  * **path →** *str* : The path section of the request URL
  * **path_with_query →** *str* : The request path + query string (if any)
  * **port →** *int* : The port section of the request URL
  * **query →** *str* : The query string part of the request URL
  * **query_args →** *Dict* : Returns the parsed query string arguments
  * **scheme →** *str* : The scheme section of the request URL
  * **url →** *str* : The full request url (scheme://host:port/path?query)
  * **value →** *Dict* : Request dictionary representation

**Methods**:

  * **content_type_includes**(**content_type_substr**: *str*) → *bool* : Returns True if the given string is a substring of the Content-Type header value
  * **has_body**() → *bool* : Returns True if the request has a non-empty body
  * **has_header**(**header**: *str*) → *bool* : Returns True if the given header name (case insensitive) is included in the requests' headers


### HttpRequestBuilder

 Immutable builder of Fizzgun's http request dictionaries

**Class methods**:

  * **new_from**(**request_dict**: *Dict*) → *HttpRequestBuilder* : Creates a new builder instance with the given initial state

**Methods**:

  * **build**() → *Dict* : Return a request dictionary representation build from the state of this builder
  * **with_body**(**body**: *str*) → *HttpRequestBuilder* : Returns a new builder instance with the given request body
  * **with_header**(**name**: *str*, **value**: *str*) → *HttpRequestBuilder* : Returns a new builder instance with the given header name and value
  * **with_headers**(**headers**: *Tuple*) → *HttpRequestBuilder* : Returns a new builder instance with all the given headers added
  * **with_host**(**host**: *str*) → *HttpRequestBuilder* : Returns a new builder instance with the given request host
  * **with_http_version**(**http_version**: *str*) → *HttpRequestBuilder* : Returns a new builder instance with the given request http version
  * **with_method**(**method**: *str*) → *HttpRequestBuilder* : Returns a new builder instance with the given request method
  * **with_path**(**path**: *str*) → *HttpRequestBuilder* : Returns a new builder instance with the given request path
  * **with_port**(**port**: *int*) → *HttpRequestBuilder* : Returns a new builder instance with the given request port
  * **with_query**(**query**: *str*) → *HttpRequestBuilder* : Returns a new builder instance with the given request query string
  * **with_query_args**(**query_args**: *Tuple*) → *HttpRequestBuilder* : Returns a new builder instance with a query string built from the given arguments
  * **with_scheme**(**scheme**: *str*) → *HttpRequestBuilder* : Returns a new builder instance with the given request scheme
  * **with_url**(**url**: *str*) → *HttpRequestBuilder* : Returns a new builder instance with updated scheme, host, port, path, and query extracted from the given url
  * **without_header**(**name**: *str*) → *HttpRequestBuilder* : Returns a new builder instance with the given header removed (if it exists)


### Expectations

 Fluent interface for defining response expectations. Usage:

    checks = Expectations()
    checks.expect('status').to.be_equal_to(200)
    checks.expect('body').to.include('hello world')

**Methods**:

  * **clone**() → *Expectations* : Returns a copy of this object
  * **expect**(**field**: *str*) → *Matcher* : Instantiates a Matcher for the given response field.
  * **extend**(**expectations**: *Expectations*) → *None* : Extends the expectations defined in this instance with all the expectations defined in the passed argument


### Matcher

 

**Properties**:

  * **not_to →** *Matcher* : Inverts the matching result. E.g. `not_to.include('this')`
  * **to →** *Matcher* : No operation, just to improve readability when fluently defining expectations

**Methods**:

  * **be_equal_to**(**value**: *Any*) → *None* : Expects the result to be successful if the target is equal to the given argument
  * **be_in**(**the_whole**: *Any*) → *None* : Expects the result to be successful if the target is included in the given list of arguments
  * **be_in_range**(**from_value**: *Any*, **to_value**: *Any*) → *None* : Expects the result to be successful if the from_value <= target <= to_value
  * **be_in_ranges**(**range_spec**: *str*) → *None* : Expects the result to be successful if the numeric target matches the given range specification.
        The specification is a list of numbers or intervals. E.g: `1,2,5-10,56,70-90`
  * **include**(**values**: *Any*) → *None* : Expects the result to be successful if the target includes all the given argument
  * **match**(**value**: *str*) → *None* : Expects the result to be successful if the target matches the given regex
