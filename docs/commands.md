# Commands

To get the list of available commands execute: `fizzgun -h`. The output looks like this:

```text
$ fizzgun -h
usage: fizzgun [-h] {run,gen-config,bubbles} ...

Fizzgun

optional arguments:
  -h, --help            show this help message and exit

Commands:
  {run,gen-config,bubbles}
    run                 Start fizzgun
    gen-config          Generate sample config file
    bubbles             Show information about existing bubbles
```

You can find out more about each command by executing `fizzgun <command> -h`


## Command: run

This is the main command. `fizzgun run` will start all the Fizzgun processes and the http proxy, load the mutation
bubbles, and wait for incoming requests.

Let's see the command help message by running `fizzgun run -h`:

```text
$ fizzgun run -h
usage: fizzgun run [-h] [-c FILE]

optional arguments:
  -h, --help            show this help message and exit
  -c FILE, --config FILE
                        Fizzgun YAML configuration file
```

As you can see, the `-c` or `--config` argument is optional. If not passed, fizzgun will look for a `fizzgun.yaml` file
in the current working directory, if there's no such a file then the default configuration settings will be used (as
described in [configuration](configuration.md)).

Just execute `fizzgun run`, the output should look like this:

```text
$ fizzgun run
Loading bubble Trimmer
Loading bubble TypeChanger
Loading bubble Injector
Loading bubble Shellshock
...
Proxy server listening at http://0.0.0.0:8888
```

You'll also see logging output here once the proxy starts receiving requests.

If you want to use specific configuration settings, you can do so by creating a `fizzgun.yaml` file or by providing a
path to a different `YAML` config file. E.g.:
`fizzgun run --config path/to/my/custom_config.yaml`


Via this configuration file you can define which bubbles are enabled, tweak individual bubble settings, reporting
settings, create incoming requests filters, set performance parameters (e.g. queue mechanisms, using threads or
processes), etc.

See the [configuration](configuration.md) section to learn about all the details.


## Command: gen-config


`fizzgun gen-config` creates a sample configuration file `fizzgun.yaml` on the local directory.
Optionally you can define a different destination via `-f` or `--filename`. E.g.

```text
$ fizzgun gen-config --filename /tmp/custom_fizzgun.yaml
File generated at: /tmp/custom_fizzgun.yaml
```

The generated file will contain some documentation and sample values that you will need to tweak to meet your needs.
Alternatively you can generate a file with Fizzgun's defaults via `fizzgun gen-config --defaults`.


## Command: bubbles

`fizzgun bubbles` shows information about the Bubbles that are loaded by a given configuration (or the default
configuration if no config file path is given).

Let's see the command's help message: `fizzgun bubbles -h`

```text
$ fizzgun bubbles -h
usage: fizzgun bubbles [-h] [-c FILE]

optional arguments:
  -h, --help            show this help message and exit
  -c FILE, --config FILE
                        Fizzgun YAML configuration file
```

The output for the default configuration looks like this:

```text
$ fizzgun bubbles

Name: Trimmer
Description: Removes parameters from requests
Tags:
  * name:trimmer
  * category:data-validation
  * data:json
  * data:querystring
  * data:x-www-form-urlencoded
Expectations:
  * Expecting 'status' to be in ranges ['0-499']
  

Name: Shellshock
Description: Attempts to exploit the shellshock bash vulnerability by injecting a specially crafted header
    (CVE-2014-6271, CVE-2014-7169, CVE-2014-7186, CVE-2014-7187, CVE-2014-6277 and CVE 2014-6278)
Tags:
  * name:shellshock
  * category:security
  * data:headers
Expectations:
  * Expecting 'status' to be in ranges ['0-499']
  * Expecting 'body' not to include ['sh3llsh0ck']
  
...
```

Via configuration some bubbles might be whitelisted/blacklisted, or you might load your own bubbles. So the output will
differ accordingly if you pass the `-c` or `--config` argument with the path to your config file or if a `fizzgun.yaml`
file already exists.
