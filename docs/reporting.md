### Location

When using the [default configuration](configuration.md) Fizzgun will create the report files in a `./reports` directory
(where `.` is the directory from which the `fizzgun run` command is executed). You can change this setting and also the
report format as described in [report configuration](configuration.md#report).

Alternatively you can access the reports by setting your browser's proxy settings to use Fizzgun's mitmproxy and
navigating to `http://fizzgun.it/reports/`.

If Fizzgun is configured to track a session identifier on the requests it receives, the resulting filename will be
`[session-identifier].txt` (or `.html`), otherwise the identifier `global-session` is used. (see the `track_sesssions`
setting of the [mitmproxy configuration](configuration.md#mitmproxy)).


### Content

When the server response of a mutant request does not meet the expectations a report entry will be created containing:
 
 * The name and description of the [bubble](bubbles.md) that generated the request.
 * A list of errors (the unmet expectations).
 * The original HTTP request (from which the mutant was generated).
 * The modified HTTP request (the mutant).
 * The server's HTTP response.
 
Here's an example of a report entry:

```text
Bubble: Enlarger
Description: Identifies parameters in requests and replaces them with larger values

Errors:
    * Expecting 'status' to be in ranges ['0-499'] (actual: 500)


    Original Request:
    -----------------

POST http://localhost:4567/item HTTP/1.1
Host: localhost:4567
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:56.0) Gecko/20100101 Firefox/56.0
Accept: application/json, text/javascript, */*; q=0.01
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://localhost:4567/
Content-Type: application/json; charset=utf-8
X-Requested-With: XMLHttpRequest
Content-Length: 65
Cookie: ajs_user_id=null; ajs_group_id=null; ajs_anonymous_id=%220b51495d-00ef-47e1-afac-a9bb853bc282%22
Connection: keep-alive

{"name":"my-item","description":"the description","quantity":"1"}


    Modified Request:
    -----------------

POST http://localhost:4567/item HTTP/1.1
Host: localhost:4567
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:56.0) Gecko/20100101 Firefox/56.0
Accept: application/json, text/javascript, */*; q=0.01
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://localhost:4567/
Content-Type: application/json; charset=utf-8
X-Requested-With: XMLHttpRequest
Content-Length: 65
Cookie: ajs_user_id=null; ajs_group_id=null; ajs_anonymous_id=%220b51495d-00ef-47e1-afac-a9bb853bc282%22
Connection: keep-alive

{"name": "my-item", "description": "the description", "quantity": "1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"}


    Server Response:
    ----------------

500 INTERNAL SERVER ERROR
Content-Type: text/html; charset=utf-8
Content-Length: 942
Server: Werkzeug/0.12.2 Python/3.6.1
Date: Tue, 17 Oct 2017 22:45:02 GMT

Traceback (most recent call last):

  File "/usr/lib/python3.6/site-packages/flask/app.py", line 1982, in wsgi_app
    response = self.full_dispatch_request()

  File "/usr/lib/python3.6/site-packages/flask/app.py", line 1614, in full_dispatch_request
    rv = self.handle_user_exception(e)

  File "/usr/lib/python3.6/site-packages/flask/app.py", line 1517, in handle_user_exception
    reraise(exc_type, exc_value, tb)

  File "/usr/lib/python3.6/site-packages/flask/_compat.py", line 33, in reraise
    raise value

  File "/usr/lib/python3.6/site-packages/flask/app.py", line 1612, in full_dispatch_request
    rv = self.dispatch_request()

  File "/usr/lib/python3.6/site-packages/flask/app.py", line 1598, in dispatch_request
    return self.view_functions[rule.endpoint](**req.view_args)

  File "shop_app.py", line 59, in create_item
    (name, description, quantity))

OverflowError: Python int too large to convert to SQLite INTEGER
```